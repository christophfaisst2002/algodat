package at.fai.projects.Bowling;

public interface BowlingGame {

        public void roll(int noOfPins);

        public int score();
    }

