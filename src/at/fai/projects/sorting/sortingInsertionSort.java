package at.fai.projects.sorting;

public class sortingInsertionSort implements Sorter {

    public int[] sort(int[] arr) {
            for (int j = 1; j < arr.length; j++) {
                int sorting = arr[j];
                int i = j - 1;
                while ((i > -1) && (arr[i] > sorting)) {
                    arr[i + 1] = arr[i];
                    i--;
                }
                arr[i + 1] = sorting;
            }
            return arr;
       }
}


