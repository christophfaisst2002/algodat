package at.fai.projects.sorting;

public class SelectionSort implements Sorter{
    public static void main(String[] args) {

    }
    @Override
    public int[] sort(int[] arr) {
            for (int i = 0; i < arr.length - 1; i++)
            {
                int sorthelp1 = i;
                for (int j = i + 1; j < arr.length; j++){
                    if (arr[j] < arr[sorthelp1]){
                        sorthelp1 = j;
                    }
                }
                int smallerNumber = arr[sorthelp1];
                arr[sorthelp1] = arr[i];
                arr[i] = smallerNumber;
            }
            return arr;
        }

    }

