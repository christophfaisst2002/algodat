package at.fai.projects.sorting;

public interface Sorter {
    public int[] sort(int[] arr);

}
