package at.fai.projects.sorting;
import java.util.*;
import java.util.Collections;

public class BucketSort {

    static void bucketSort(float arr[], int n)
    {
        if (n <= 0)
            return;
        Vector<Float>[] buckets = new Vector[n];

        for (int i = 0; i < n; i++) {
            buckets[i] = new Vector<Float>();
        }

        for (int i = 0; i < n; i++) {
            float index = arr[i] * n;
            buckets[(int)index].add(arr[i]);
        }

        for (int i = 0; i < n; i++) {
            Collections.sort(buckets[i]);
        }

        int index = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < buckets[i].size(); j++) {
                arr[index++] = buckets[i].get(j);
            }
        }
    }

    public static void main(String args[])
    {
        float arr[] = { (float) 0.27, (float) 0.35,
                (float)0.483, (float)0.124,
                (float)0.352, (float)0.351, (float)0.871 };

        int n = arr.length;
        bucketSort(arr, n);
        for (float result : arr) {
            System.out.print(" | " + result + " | ");
        }
    }
}



