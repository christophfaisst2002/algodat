package at.fai.projects.sorting;

public class Main  {
    public static void main(String[] args) {

      int[] arr = SortHelper.generateArray(10);
      Sorter sorter = new sortingBubbleSort();
      printArray(arr);
      int[] sorted = sorter.sort(arr);
      printArray(sorted);

      Sorter insertion = new sortingInsertionSort();
      insertion.sort(arr);
      int[] sorted2 = sorter.sort(arr);
      printArray(sorted2);

      Sorter selection = new SelectionSort();
      selection.sort(arr);
      int[] sorted3 = sorter.sort(arr);
      printArray(sorted3);


    }

    private static void printArray(int[] arr){
        for (int value: arr) {
            System.out.print(value + " - ");
        }
        System.out.println();
        System.out.println();
    }
}
