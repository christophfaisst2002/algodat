package at.fai.projects.stack;

import at.fai.projects.stack.exceptions.StackEmptyException;

import java.util.Stack;

public class main {
    public static void main(String[] args) {
        FaiStack stack = new FaiStack();
        stack.push(12);
        stack.push(27);
        stack.push(14);
        stack.push(34);
        try {
            System.out.println(stack.pop());
            System.out.println(stack.pop());
            System.out.println(stack.pop());
            System.out.println(stack.pop());

        } catch (StackEmptyException e) {
            System.out.println(e.getMessage());
        }
    }
}
