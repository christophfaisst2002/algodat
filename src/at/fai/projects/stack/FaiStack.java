package at.fai.projects.stack;

import at.fai.projects.list.Node;
import at.fai.projects.stack.exceptions.StackEmptyException;

import java.util.Stack;

public class FaiStack {
    private Node top;
   public void push(int value){
        Node newNode = new Node(value);
        if(top==null){
            this.top = newNode;
        }else{
            newNode.setNext(this.top);
            this.top = newNode;
        }
    }

    public int pop() throws StackEmptyException {
        if(this.top == null){
            throw new StackEmptyException("Stack is empty");
        }
        Node oldNode = this.top;
        this.top = oldNode.getNext();

        return oldNode.getValue();
    }
}
