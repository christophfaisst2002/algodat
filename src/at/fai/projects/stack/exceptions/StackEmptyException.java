package at.fai.projects.stack.exceptions;

public class StackEmptyException extends Exception{
    public StackEmptyException(String message) {
        super(message);
    }
}
