package at.fai.projects.list;

interface HTLList {
    public void add(int value);
    public int get(int index);
    public void remove(int index);
    public void clear();
}