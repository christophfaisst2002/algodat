package at.fai.projects.list;

public class FaiList implements HTLList{

    private Node root;

    @Override
    public void add(int value) {
        Node n = new Node(value);
        if (root == null) {
            root = n;
        } else {
            getLastNode().next = n;
        }
    }

    private Node getLastNode() {
        Node n = root;
        while (n.getNext() != null) {
            n = n.next;
        }
        return n;
    }

    @Override
    public int get(int index) {
        Node n = root;
        int i = 0;
        while (i<index){
            if (n.next==null){
                throw new IndexOutOfBoundsException("No data");
            }
            n = n.next;
            i++;
        }
        if (root==null){
            throw new IndexOutOfBoundsException("Root is null");
        }
        return n.getValue();
    }

    @Override
    public void remove(int index) {
    Node n = root;
    int count = 1;
    while( count < index - 1){
        n = n.next;
        count++;

    }
    }

    @Override
    public void clear() {
        root = null;
    }

}
